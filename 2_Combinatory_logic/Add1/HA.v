module HA(a,b,out);
    input wire a;
	input wire b;
    output wire [1:0] out;
	nand(out[1],a,b);
	nand(w1,a,out[1]);
	nand(w2,b,out[1]);
	nand(out[0],w1,w2);
endmodule
